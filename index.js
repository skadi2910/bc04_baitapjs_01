// BÀI 1
function getSalaries() {
  var wageperDay = parseFloat(document.getElementById("txt-wages").value);
  console.log("wageperDay: ", wageperDay);
  var days = document.getElementById("txt-days").value * 1;
  console.log("days: ", days);
  var salaries = wageperDay * days;
  console.log("salaries: ", salaries);

  resultsEl = document.getElementById("txt-results1").innerText =
    "Tiền lương của bạn là: " + new Intl.NumberFormat("vn-VN").format(salaries);
}

// BÀI 2
function getAverage() {
  var num1 = parseFloat(document.getElementById("txt-num1").value);
  var num2 = parseFloat(document.getElementById("txt-num2").value);
  var num3 = parseFloat(document.getElementById("txt-num3").value);
  var num4 = parseFloat(document.getElementById("txt-num4").value);
  var num5 = parseFloat(document.getElementById("txt-num5").value);
  // NORMAL
  var sum = num1 + num2 + num3 + num4 + num5;
  console.log("sum: ", sum);
  var average = sum / 5;
  console.log("average: ", average);
  //  ARRAY
  var num_arr = [];
  num_arr.push(num1, num2, num3, num4, num5);
  console.log("num_arr: ", num_arr);
  var num_arr_sum = num_arr.reduce(function (a, b) {
    return a + b;
  }, 0);
  console.log("num_arr_sum: ", num_arr_sum);
  var num_arr_avg = num_arr_sum / num_arr.length;
  console.log("num_arr_avg: ", num_arr_avg);

  resultsEl = document.getElementById("txt-results2").innerHTML =
    "Tổng: " + sum + " <br />  Trung bình: " + average;
}

// BÀI 3
function getVNDfromUSD() {
  var exchangeRate = 23500;
  var usDollar = Math.abs(document.getElementById("txt-usd").value);
  console.log("usDollar: ", usDollar);
  var vnDong = usDollar * exchangeRate;
  console.log("vnDong: ", vnDong);

  resultsEl = document.getElementById("txt-results3").innerText =
    new Intl.NumberFormat("vn-VN", {
      style: "currency",
      currency: "VND",
    }).format(vnDong);
}

// BÀI 4
function getPerimeter() {
  var r_width = Math.abs(document.getElementById("txt-width").value);
  console.log("r_width: ", r_width);
  var r_length = Math.abs(document.getElementById("txt-height").value);
  console.log("r_length: ", r_length);
  var perimeter = (r_width + r_length) * 2;
  console.log("perimeter: ", perimeter);
  resultsEl = document.getElementById("txt-results4_1").innerHTML =
    "Chu Vi: " + perimeter;
}

function getArea() {
  var r_width = Math.abs(document.getElementById("txt-width").value);
  var r_length = Math.abs(document.getElementById("txt-height").value);
  var area = r_width * r_length;
  console.log("area: ", area);
  resultsEl = document.getElementById("txt-results4_2").innerHTML =
    "Diện Tích: " + area;
}

// BÀI 5
function getSum_ofNumber() {
  var num = parseInt(document.getElementById("txt-num").value);
  console.log("num: ", num);
  var unit = num % 10;
  console.log("unit: ", unit);
  var tenth = parseInt((num / 10) % 10);
  console.log("tenth: ", tenth);
  var hundreth = parseInt(num / 100);
  console.log("hundreth: ", hundreth);
  var sum = unit + tenth + hundreth;
  console.log("sum: ", sum);

  if (hundreth != 0) {
    resultsEl = document.getElementById("txt-results5").innerHTML =
      "Sum: " + hundreth + " + " + tenth + " + " + unit + " = " + sum;
  } else {
    resultsEl = document.getElementById("txt-results5").innerHTML =
      "Sum: " + tenth + " + " + unit + " = " + sum;
  }
}
